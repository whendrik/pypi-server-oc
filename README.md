# PyPi Server for OpenShift

PyPi Server compatible with openshift `new-app` command

## Run on Open-Shift

Create the app, optionally use `--name=pypi-server`
```
oc new-app https://gitlab.com/whendrik/pypi-server-oc.git
```

Log the build process
```
oc logs -f bc/pypi-server
```

Check which `CLUSTER_IP` is our server
```
oc get svc
```

Optional - expose the service by creating a route

```
oc expose svc/dofa
```

Delete with

```
oc delete all --selector app=pypi-server-oc
```